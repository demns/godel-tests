import { Selector } from 'testcafe'; // first import testcafe selectors

fixture `Simple input test`// declare the fixture
    .page `localhost:3000`;  // specify the start page


//then create a test and place your code there
test('My first test', async t => {
    await t
        .typeText('#input', 'here is some long input')
        .click('#submit-button')

        // Use the assertion to check if the actual header text is equal to the expected one
        .expect(Selector('#submitted-string').innerText).eql('here is some long input submitted!');
});